﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem.Models
{
	public interface IPrintable
	{
		void Print();
	}
}
