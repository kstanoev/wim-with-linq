﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem.Models
{
	public interface ILoggable : IPrintable
	{
		IEnumerable<LogItem> ActivityHistory { get; }
	}
}
