﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem.Models
{
	public enum Seniority
	{
		Senior = 3,
		Regular = 2,
		Junior = 1
	}
}
