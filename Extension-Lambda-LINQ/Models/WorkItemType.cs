﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem.Models
{
	// Ordered by importance
	public enum WorkItemType
	{
		Bug = 2,
		Feature = 1,
		None = 0
	}
}
