﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem.Models
{
	public class Member : ILoggable
	{
		private List<WorkItem> assignedWorkItems = new List<WorkItem>();
		private List<LogItem> activityHistory = new List<LogItem>();

		public Member(string name, Seniority seniority)
		{
			this.Name = name;
			this.Seniority = seniority;
			this.activityHistory.Add(new LogItem($"{name} created"));
		}

		public string Name { get; private set; }
		public Seniority Seniority { get; private set; }
		public IEnumerable<WorkItem> AssignedWorkItems
		{
			get
			{
				// TODO: Improve Member.AssignedWorkItems encapsulation
				return this.assignedWorkItems;
			}
		}
		public IEnumerable<LogItem> ActivityHistory
		{
			get
			{
				// TODO: Improve Member.AssignedWorkItems encapsulation
				return this.activityHistory;
			}
		}

		public void AssignWorkItem(WorkItem workItem)
		{
			// Code smell
			this.assignedWorkItems.Add(workItem);
			this.activityHistory.Add(new LogItem($"{workItem.Title} added to {this.Name}"));
		}

		public void Print()
		{
			Console.WriteLine(this.Name);
		}
	}
}
