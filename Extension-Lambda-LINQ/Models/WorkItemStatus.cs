﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem.Models
{
	public enum WorkItemStatus
	{
		NotStarted = 1,
		InProgress = 2,
		ReadyForTest = 3,
		Done = 4
	}
}
