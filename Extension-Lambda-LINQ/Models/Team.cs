﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace WorkItemManagementSystem.Models
{
	public class Team : ILoggable
	{
		private List<Member> members = new List<Member>();
		private List<WorkItem> workItems = new List<WorkItem>();
		private List<LogItem> activityHistory = new List<LogItem>();

		public Team(string name)
		{
			this.Name = name;
			this.activityHistory.Add(new LogItem($"{name} created"));
		}
		public Team(string name, IEnumerable<WorkItem> workItems) : this(name)
		{
			this.workItems = new List<WorkItem>(workItems);
			// TODO: Use ForEach
			foreach (var item in workItems)
			{
				this.activityHistory.Add(new LogItem($"{item.Title} created"));
			}
		}

		public string Name
		{
			get;
			private set;
		}
		public IEnumerable<Member> Members
		{
			get
			{
				return new List<Member>(this.members);
			}
		}
		public IEnumerable<WorkItem> WorkItems
		{
			get
			{
				// TODO: Improve Team.WorkItems encapsulation
				return this.workItems;
			}
		}
		public IEnumerable<LogItem> ActivityHistory
		{
			get
			{
				// TODO: Improve Team.ActivityHistory encapsulation
				return this.activityHistory;
			}
		}

		public void AddWorkItem(WorkItem workItem)
		{
			this.workItems.Add(workItem);
			this.activityHistory.Add(new LogItem($"{workItem} added to {this.Name}"));
		}
		public void AddMember(Member member)
		{
			this.members.Add(member);
			this.activityHistory.Add(new LogItem($"{member.Name} added to {this.Name}"));
		}
		public Member FindMember(Predicate<Member> searchPredicate)
		{
			return this.members.Where(m => searchPredicate(m)).FirstOrDefault();
		}

		public void Print()
		{
			Console.WriteLine($"{this.Name}");
		}
	}
}
