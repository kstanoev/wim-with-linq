﻿using WorkItemManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem
{
	public class Engine
	{
		// TODO: Update engine
		private static List<Team> teams = new List<Team>();

		static Engine()
		{
			// Team 1 Стилиян Костадинов, Кирил Кузманов, Станислав Динев
			var team1 = new Team("Team 1", GetRandomWorkItems());
			team1.AddMember(new Member("S. Kostadinov", GetRandomSeniority()));
			team1.AddMember(new Member("K. Kuzmanov", GetRandomSeniority()));
			team1.AddMember(new Member("S. Dinev", GetRandomSeniority()));
			teams.Add(team1);
			// Team 2 Анастас Костов, Веселин Игнатов, Станислав Георгиев
			var team2 = new Team("Team 2", GetRandomWorkItems());
			team2.AddMember(new Member("A. Kostov", GetRandomSeniority()));
			team2.AddMember(new Member("V. Ignatov", GetRandomSeniority()));
			team2.AddMember(new Member("S. Georgiev", GetRandomSeniority()));
			teams.Add(team2);
			//	Team 3	Тихомир Тодоров Мария Иванова   Теодора Димитрова
			var team3 = new Team("Team 3", GetRandomWorkItems());
			team3.AddMember(new Member("T. Todorov", GetRandomSeniority()));
			team3.AddMember(new Member("M. Ivanova", GetRandomSeniority()));
			team3.AddMember(new Member("T. Dimitrova", GetRandomSeniority()));
			teams.Add(team3);
			//	Team 4	Антония Петрова Мартин Манев    Антон Гервазиев
			var team4 = new Team("Team 4", GetRandomWorkItems());
			team4.AddMember(new Member("A. Petrova", GetRandomSeniority()));
			team4.AddMember(new Member("M. Manev", GetRandomSeniority()));
			team4.AddMember(new Member("A. Gervaziev", GetRandomSeniority()));
			teams.Add(team4);
			//	Team 5	Драгомир Драгнев    Стефан Иванов   Петър Чавдаров
			var team5 = new Team("Team 5", GetRandomWorkItems());
			team5.AddMember(new Member("D. Dragnev", GetRandomSeniority()));
			team5.AddMember(new Member("S. Ivanov", GetRandomSeniority()));
			team5.AddMember(new Member("P. Chavdarov", GetRandomSeniority()));
			teams.Add(team5);
			//	Team 6	Николай Никифоров   Диана Крумова   Димитър Грозданов
			var team6 = new Team("Team 6", GetRandomWorkItems());
			team6.AddMember(new Member("N. Nikiforov", GetRandomSeniority()));
			team6.AddMember(new Member("D. Krumova", GetRandomSeniority()));
			team6.AddMember(new Member("D. Grozdanov", GetRandomSeniority()));
			teams.Add(team6);
			//	Team 7	Димитър Михов   Петър Пенев Васил Проданов
			var team7 = new Team("Team 7", GetRandomWorkItems());
			team7.AddMember(new Member("D. Mihov", GetRandomSeniority()));
			team7.AddMember(new Member("P. Penev", GetRandomSeniority()));
			team7.AddMember(new Member("V. Prodanov", GetRandomSeniority()));
			teams.Add(team7);
			//	Team 8	Венцислав Чернев    Петър Ботев Венцислав Колев
			var team8 = new Team("Team 8", GetRandomWorkItems());
			team8.AddMember(new Member("V. Chernev", GetRandomSeniority()));
			team8.AddMember(new Member("P. Botev", GetRandomSeniority()));
			team8.AddMember(new Member("V. Kolev", GetRandomSeniority()));
			teams.Add(team8);
			//	Team 9	Александър Евангелатов  Ива Жечкова Весела Манолова
			var team9 = new Team("Team 9", GetRandomWorkItems());
			team9.AddMember(new Member("A. Evangelatov", GetRandomSeniority()));
			team9.AddMember(new Member("I. Zhechkova", GetRandomSeniority()));
			team9.AddMember(new Member("V. Manolova", GetRandomSeniority()));
			teams.Add(team9);
			//	Team 10	Васил Йошовски  Руси Шапкаджиев Владимир Иванчов
			var team10 = new Team("Team 10", GetRandomWorkItems());
			team10.AddMember(new Member("V. Yoshovski", GetRandomSeniority()));
			team10.AddMember(new Member("R.Shapkadjiev", GetRandomSeniority()));
			team10.AddMember(new Member("V. Ivanchov", GetRandomSeniority()));
			teams.Add(team10);
			//	Team 11	Петър Янев  Селим Ахмедов   Георги Десполов
			var team11 = new Team("Team 11", GetRandomWorkItems());
			team11.AddMember(new Member("P. Yanev", GetRandomSeniority()));
			team11.AddMember(new Member("S. Ahmedov", GetRandomSeniority()));
			team11.AddMember(new Member("G.Despolov", GetRandomSeniority()));
			teams.Add(team11);
			//	Team 12	Христина Кондева    Станислав Калинов   Павел Пеловски
			var team12 = new Team("Team 12", GetRandomWorkItems());
			team12.AddMember(new Member("H. Kondeva", GetRandomSeniority()));
			team12.AddMember(new Member("S. Kalinov", GetRandomSeniority()));
			team12.AddMember(new Member("P. Pelovski", GetRandomSeniority()));
			teams.Add(team12);
			//	Team 13	Христо Конов    Абду Омар   Зорница Филипова
			var team13 = new Team("Team 13", GetRandomWorkItems());
			team13.AddMember(new Member("H. Konov", GetRandomSeniority()));
			team13.AddMember(new Member("A. Omar", GetRandomSeniority()));
			team13.AddMember(new Member("Z. Filipova", GetRandomSeniority()));
			teams.Add(team13);
			//	Team 14	Станил Димитров Иван Димов  Ивайло Стоянов
			var team14 = new Team("Team 14", GetRandomWorkItems());
			team14.AddMember(new Member("S. Dimitrov", GetRandomSeniority()));
			team14.AddMember(new Member("I. Dimov", GetRandomSeniority()));
			team14.AddMember(new Member("I. Stoyanov", GetRandomSeniority()));
			teams.Add(team14);
			//	Team 15	Лиляна Шапкаджиева  Зорница Тодорова
			var team15 = new Team("Team 15", GetRandomWorkItems());
			team15.AddMember(new Member("L. Shapkadjieva", GetRandomSeniority()));
			team15.AddMember(new Member("Z. Todorova", GetRandomSeniority()));
			team15.AddMember(new Member("l33t. Hax0r", GetRandomSeniority()));
			teams.Add(team15);
		}

		public static IEnumerable<Team> Teams
		{
			get
			{
				return teams;
			}
		}

		static string[] tasks = new string[] {
			"OperationsRequirements: Create a new person",
			"OperationsRequirements: Show all people",
			"OperationsRequirements: Show person's activity",
			"OperationsRequirements: Create a new team",
			"OperationsRequirements: Show all teams",
			"OperationsRequirements: Show team's activity",
			"OperationsRequirements: Add person to team",
			"OperationsRequirements: Show all team members",
			"OperationsRequirements: Create a new board in a team",
			"OperationsRequirements: Use data encapsulation",
			"OperationsRequirements: Properly use inheritance and polymorphism",
			"OperationsRequirements: Properly use interfaces and abstract classes",
			"OperationsRequirements: Properly use static members",
			"OperationsRequirements: Show all team boards",
			"OperationsRequirements: Show board's activity",
			"OperationsRequirements: Create a new Bug/Story/Feedback in a board",
			"OperationsRequirements: Change Priority/Severity/Status of a bug",
			"OperationsRequirements: Change Priority/Size/Status of a story",
			"OperationsRequirements: Change Rating/Status of a feedback",
			"OperationsRequirements: Assign/Unassign work item to a person",
			"OperationsRequirements: Add comment to a work item",
			"OperationsRequirements: List work items with options:List all, Filter bugs/stories/feedback only",
			"OperationsRequirements: List work items with options:Filter by status and/or assignee",
			"OperationsRequirements: List work items with options:Sort by title/priority/severity/size/rating",
			"BugRequirements: Title is a string between 10 and 50 symbols.",
			"BugRequirements: Description is a string between 10 and 500 symbols.",
			"BugRequirements: Steps to reproduce is a list of strings.",
			"BugRequirements: Priority is one of the following: High, Medium, Low",
			"BugRequirements: Severity is one of the following: Critical, Major, Minor",
			"BugRequirements: Status is one of the following: Active, Fixed",
			"BugRequirements: Assignee is a member from the team.",
			"BugRequirements: Comments is a list of comments (string messages with author).",
			"BugRequirements: History is a list of all changes (string messages) that were done to the bug.",
			"BugRequirements: Title is a string between 10 and 50 symbols.",
			"StoryRequirements: Description is a string between 10 and 500 symbols.",
			"StoryRequirements: Priority is one of the following: High, Medium, Low",
			"StoryRequirements: Size is one of the following: Large, Medium, Small",
			"StoryRequirements: Status is one of the following: NotDone, InProgress, Done",
			"StoryRequirements: Assignee is a member from the team.",
			"StoryRequirements: Comments is a list of comments (string messages with author).",
			"StoryRequirements: History is a list of all changes (string messages) that were done to the story.",
			"FeedbackRequirements: Title is a string between 10 and 50 symbols.",
			"FeedbackRequirements: Description is a string between 10 and 500 symbols.",
			"FeedbackRequirements: Rating is an integer.",
			"FeedbackRequirements: Status is one of the following: New, Unscheduled, Scheduled, Done",
			"FeedbackRequirements: Comments is a list of comments (string messages with author).",
			"FeedbackRequirements: History is a list of all changes (string messages) that were done to the feedback.",
			"Create WIM architecture",
			"Implement adding a new team",
			"Enable logging bugs to the tracking system",
			"Cannot add more than one bug",
			"Implement adding a board",
			"Adding a work item to a board throws an exception.",
			"Work item title should be between 10 and 50 symbols.",
			"Items in WorkItem.ActivityHistory are not sorted.",
		};

		private static Random random = new Random(DateTime.Now.Millisecond);
		private static Seniority GetRandomSeniority()
		{
			return (Seniority)random.Next(1, 4);
		}
		private static List<WorkItem> GetRandomWorkItems()
		{
			var numberOfItems = random.Next(15, tasks.Length);
			var workItems = new List<WorkItem>(numberOfItems);
			for (int i = 0; i < numberOfItems; i++)
			{
				workItems.Add(new WorkItem(tasks[random.Next(0, tasks.Length)], (WorkItemType)random.Next(1, 3)));
			}
			return workItems;
		}
	}
}
