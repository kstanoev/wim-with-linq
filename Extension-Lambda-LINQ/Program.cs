﻿using WorkItemManagementSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using WorkItemManagementSystem.Models;
using WorkItemManagementSystem.Extensions;
using System.Linq;

namespace WorkItemManagementSystem
{
	public class Program
	{
		public static void Main()
		{
			var team = Engine.Teams.First();
			var member = team.FindMember(m => m.Name.Contains("Kuz"));
			Console.WriteLine(member.Name);
		}
	}
}
