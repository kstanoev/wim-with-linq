﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagementSystem
{
	public static class PlaygroundExtensions
	{
		// =================================================
		// ENCAPSULATION
		// =================================================
		//foreach (var workitem in team.WorkItems)
		//{
		//	Console.WriteLine($"{workitem.Type}: {workitem.Title}");
		//}

		//foreach (var workitem in team.WorkItems)
		//{
		//	workitem.Title = workitem.Title.Replace("Cannot", "Can");
		//	workitem.UpdateType(WorkItemType.Feature);
		//}

		//foreach (var workitem in team.WorkItems)
		//{
		//	Console.WriteLine($"{workitem.Type}: {workitem.Title}");
		//}
		//===================================================
		// PREDICATE
		// ==================================================

		//===================================================
		// FUNC
		// ==================================================
		//Func<Team, bool> func = new Func<Team, bool>(Filter);
		//var result = Engine.Teams.Where(func).First();
		// ---
		//Func<Team, bool> func = t => t.Name == "Team 2";
		//var result = Engine.Teams.Where(func).First();
		// ---
		//var result = Engine.Teams.Where(t => t.Name == "Team 2").First();
		// ---
		//var result = Engine.Teams.Where(t => Filter(t)).First();
		//var result = Engine.Teams.Where(t =>
		//{
		//	return t.Name.Contains("2");
		//}).First();
		// ======================================================
		// Action
		// ======================================================
		//Engine.Teams
		//			.OrderBy(team => team.WorkItems.Count())
		//			.ToList().ForEach(t =>
		//			{
		//	Console.WriteLine(t.Name);

		//});
		// =====================================
		// Custom extension methods
		// =====================================
		//var myArray = new int[] { 1, 2, 3, 4, 5 };
		//Console.WriteLine(myArray.Sum());

		//public static int Sum(this int[] collection)
		//{
		//	int sum = 0;
		//	foreach (var item in collection)
		//	{
		//		sum += item;
		//	}

		//	return sum;
		//}
	}
}
