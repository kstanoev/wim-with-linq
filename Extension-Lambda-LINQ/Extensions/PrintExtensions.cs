﻿using WorkItemManagementSystem;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagementSystem.Models;

namespace WorkItemManagementSystem.Extensions
{
	public static class PrintExtensions
	{
		public static void PrettyPrint(this WorkItem workItem)
		{
			// TODO: Extension method that prints the status, type and title of a WorkItem
			// TODO: WorkItem.Title should be trimmed after the first N characters and end with ...
			// TODO: Print bugs in red, features in blue
			if (workItem.Type == WorkItemType.Bug)
				Console.ForegroundColor = ConsoleColor.DarkRed;
			Console.WriteLine($"[{workItem.Status}]{workItem.Type}: " +
				$"{workItem.Title.Substring(0, 20)}...");
			Console.ResetColor();
		}

		public static void Print(this IEnumerable<WorkItem> workItem)
		{
		}
	}
}
