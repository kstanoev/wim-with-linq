﻿using WorkItemManagementSystem;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagementSystem.Models;

namespace WorkItemManagementSystem.Extensions
{
	public static class EnumExtensions
	{
		// TODO: Extension method that converts a string to WorkItemType enum
		public static WorkItemType ToEnum(this string input)
		{
			WorkItemType result;
			bool success = Enum.TryParse<WorkItemType>(input, out result);
			return success ? result : WorkItemType.None;
		}

		// TODO: Extension method that returns the next/previous stage in 
		// [NotStarted, InProgress, ReadyForTest, Done]

	}
}
