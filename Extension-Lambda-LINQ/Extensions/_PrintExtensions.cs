﻿using WorkItemManagementSystem;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagementSystem.Models;

namespace WorkItemManagementSystem.Extensions
{
	public static class PrintExtensions
	{
		// Extension method that prints the status, type and title of a WorkItem
		// WorkItem.Title should be trimmed after the first N characters and end with ...
		// Print bugs in red
		public static void Print(this WorkItem workItem)
		{
			if (workItem.Type == WorkItemType.Bug)
				Console.ForegroundColor = ConsoleColor.DarkRed;
			Console.WriteLine($"[{workItem.Status}]{workItem.Type}: " +
				$"{workItem.Title.Substring(0, 20)}...");
			Console.ResetColor();
		}
	}
}
