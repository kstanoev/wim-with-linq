﻿using WorkItemManagementSystem;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagementSystem.Models;

namespace WorkItemManagementSystem.Extensions
{
	public static class EnumExtensions
	{
		// Extension method that converts a string to WorkItemType enum
		public static WorkItemType ToEnum(this string value)
		{
			WorkItemType result = WorkItemType.None;
			Enum.TryParse(value, out result);
			return result;
		}

		// Extension method that returns the next/previous stage in 
		// [NotStarted, InProgress, ReadyForTest, Done]
		public static WorkItemStatus GetNextStage(this WorkItemStatus currentStatus)
		{
			return currentStatus + 1 <= WorkItemStatus.Done ? currentStatus + 1 : WorkItemStatus.Done;
		}
	}
}
