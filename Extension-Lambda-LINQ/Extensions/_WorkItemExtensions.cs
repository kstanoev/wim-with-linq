﻿using WorkItemManagementSystem;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagementSystem.Models;
using System.Linq;

namespace WorkItemManagementSystem.Extensions
{
	public static class WorkItemExtensions
	{
		// Extension method that orders by WorkItemType importance
		public static IEnumerable<WorkItem> OrderByImportance(this IEnumerable<WorkItem> workItems)
		{
			return workItems.OrderByDescending(item => item.Type);

			return from workItem in workItems
				   orderby workItem.Type descending
				   select workItem;
		}
	}
}
